<?php

namespace Ls\GasStationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="`order`")
 * @ORM\Entity
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     *
     * @var string
     */
    private $documentExternalId;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $documentNumber;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $gasStationId;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $documentSymbol;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $driverFullName;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $carRegistrationNumber;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $date;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GasStationBundle\Entity\Client",
     *     inversedBy="orders"
     * )
     * @ORM\JoinColumn(
     *     name="client_id",
     *     referencedColumnName="id"
     * )
     *
     * @var \Ls\GasStationBundle\Entity\Client
     */
    private $client;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\GasStationBundle\Entity\LineItem",
     *   mappedBy="order",
     *   cascade={"persist", "remove"}
     * )
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $lineItems;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->lineItems = new ArrayCollection();
    }

    public function __toString()
    {
        return empty($this->getTitle()) ? '' : $this->getTitle();
    }

    /**
     * Wyniki mogą być błędne przez float.
     *
     * @return float
     */
    public function getTotalPriceNet()
    {
        $price = 0;

        foreach ($this->lineItems as $item) {
            $price += $item->getTotalPriceNet();
        }

        return $price;
    }

    /**
     * Wyniki mogą być błędne przez float.
     *
     * @return float
     */
    public function getTotalPriceGross()
    {
        $price = 0;

        foreach ($this->lineItems as $item) {
            $price += $item->getTotalPriceGross();
        }

        return $price;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentExternalId.
     *
     * @param string $documentExternalId
     *
     * @return Order
     */
    public function setDocumentExternalId($documentExternalId)
    {
        $this->documentExternalId = $documentExternalId;
        $parts = explode('/', $documentExternalId);

        if (count($parts) === 3) {
            $this->documentNumber = $parts[0];
            $this->documentSymbol = $parts[2];
            $this->gasStationId = $parts[1];
        } elseif (count($parts) === 2) {
            $this->documentNumber = $parts[0];
            $this->documentSymbol = $parts[1];
        }

        return $this;
    }

    /**
     * Get documentExternalId.
     *
     * @return string
     */
    public function getDocumentExternalId()
    {
        return $this->documentExternalId;
    }

    /**
     * Set documentNumber.
     *
     * @param string $documentNumber
     *
     * @return Order
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;

        return $this;
    }

    /**
     * Get documentNumber.
     *
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * Set gasStationId.
     *
     * @param string $gasStationId
     *
     * @return Order
     */
    public function setGasStationId($gasStationId)
    {
        $this->gasStationId = $gasStationId;

        return $this;
    }

    /**
     * Get gasStationId.
     *
     * @return string
     */
    public function getGasStationId()
    {
        return $this->gasStationId;
    }

    /**
     * Set documentSymbol.
     *
     * @param string $documentSymbol
     *
     * @return Order
     */
    public function setDocumentSymbol($documentSymbol)
    {
        $this->documentSymbol = $documentSymbol;

        return $this;
    }

    /**
     * Get documentSymbol.
     *
     * @return string
     */
    public function getDocumentSymbol()
    {
        return $this->documentSymbol;
    }

    /**
     * Set carRegistrationNumber.
     *
     * @param string $carRegistrationNumber
     *
     * @return Order
     */
    public function setCarRegistrationNumber($carRegistrationNumber)
    {
        $this->carRegistrationNumber = $carRegistrationNumber;

        return $this;
    }

    /**
     * Get carRegistrationNumber.
     *
     * @return string
     */
    public function getCarRegistrationNumber()
    {
        return $this->carRegistrationNumber;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Order
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Order
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set client.
     *
     * @param \Ls\GasStationBundle\Entity\Client $client
     *
     * @return Order
     */
    public function setClient(\Ls\GasStationBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return \Ls\GasStationBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add lineItem.
     *
     * @param \Ls\GasStationBundle\Entity\LineItem $lineItem
     *
     * @return Order
     */
    public function addLineItem(\Ls\GasStationBundle\Entity\LineItem $lineItem)
    {
        $this->lineItems[] = $lineItem;
        $lineItem->setOrder($this);

        return $this;
    }

    /**
     * Remove lineItem.
     *
     * @param \Ls\GasStationBundle\Entity\LineItem $lineItem
     */
    public function removeLineItem(\Ls\GasStationBundle\Entity\LineItem $lineItem)
    {
        $this->lineItems->removeElement($lineItem);
    }

    /**
     * Get lineItems.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLineItems()
    {
        return $this->lineItems;
    }

    /**
     * Set driverFullName.
     *
     * @param string $driverFullName
     *
     * @return Order
     */
    public function setDriverFullName($driverFullName)
    {
        $this->driverFullName = $driverFullName;

        return $this;
    }

    /**
     * Get driverFullName.
     *
     * @return string
     */
    public function getDriverFullName()
    {
        return $this->driverFullName;
    }
}
