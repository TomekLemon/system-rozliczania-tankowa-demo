<?php

namespace Ls\GasStationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="line_item")
 * @ORM\Entity
 */
class LineItem
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GasStationBundle\Entity\Order",
     *     inversedBy="lineItems"
     * )
     * @ORM\JoinColumn(
     *     name="order_id",
     *     referencedColumnName="id"
     * )
     *
     * @var \Ls\GasStationBundle\Entity\Order
     */
    private $order;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GasStationBundle\Entity\Product"
     * )
     * @ORM\JoinColumn(
     *     name="product_id",
     *     referencedColumnName="id"
     * )
     *
     * @var \Ls\GasStationBundle\Entity\Product
     */
    private $product;

    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * @var decimal
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * @var decimal
     */
    private $totalPriceNet;

    /**
     * @ORM\Column(type="decimal", scale=2)
     *
     * @var decimal
     */
    private $totalPriceGross;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity.
     *
     * @param string $quantity
     *
     * @return OrderPosition
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set totalPriceNet.
     *
     * @param string $totalPriceNet
     *
     * @return OrderPosition
     */
    public function setTotalPriceNet($totalPriceNet)
    {
        $this->totalPriceNet = $totalPriceNet;

        return $this;
    }

    /**
     * Get totalPriceNet.
     *
     * @return string
     */
    public function getTotalPriceNet()
    {
        return $this->totalPriceNet;
    }

    /**
     * Set totalPriceGross.
     *
     * @param string $totalPriceGross
     *
     * @return OrderPosition
     */
    public function setTotalPriceGross($totalPriceGross)
    {
        $this->totalPriceGross = $totalPriceGross;

        return $this;
    }

    /**
     * Get totalPriceGross.
     *
     * @return string
     */
    public function getTotalPriceGross()
    {
        return $this->totalPriceGross;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return OrderPosition
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return OrderPosition
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set order.
     *
     * @param \Ls\GasStationBundle\Entity\Order $order
     *
     * @return OrderPosition
     */
    public function setOrder(\Ls\GasStationBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return \Ls\GasStationBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product.
     *
     * @param \Ls\GasStationBundle\Entity\Product $product
     *
     * @return OrderPosition
     */
    public function setProduct(\Ls\GasStationBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product.
     *
     * @return \Ls\GasStationBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
