<?php

namespace Ls\GasStationBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService
{
    private $container;
    private $authorizationChecker;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->authorizationChecker = $container->get('security.authorization_checker');
    }

    public function addToMenu(ItemInterface $parent, $route, $set)
    {
        $orderItem = $parent->addChild('Zamówienia', array(
            'route' => 'ls_gas_station_order_list_admin',
        ));

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $clientItem = $parent->addChild('Klienci', array(
                'route' => 'ls_gas_station_client_list_admin',
            ));
        }

        $current_set = true;

        if (!$set) {
            return $current_set;
        }

        switch ($route) {
            case 'ls_gas_station_order_list_admin':
                $orderItem->setCurrent(true);
                break;
            case 'ls_gas_station_client_list_admin':
            case 'ls_gas_station_client_create_admin':
            case 'ls_gas_station_client_edit_admin':
            case 'ls_gas_station_client_batch_admin':
                if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                    $clientItem->setCurrent(true);
                }

                break;
            default:
                $current_set = false;
                break;
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent)
    {
        $router = $this->container->get('router');

        $orders = new AdminRow('Zamówienia');
        $orders->addLink(new AdminLink('Przeglądaj', 'glyphicon-list', $router->generate('ls_gas_station_order_list_admin')));
        $parent->addRow($orders);

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $clients = new AdminRow('Klienci');
            $clients->addLink(new AdminLink('Przeglądaj', 'glyphicon-list', $router->generate('ls_gas_station_client_list_admin')));
            $clients->addLink(new AdminLink('Dodaj', 'glyphicon-list', $router->generate('ls_gas_station_client_create_admin')));
            $parent->addRow($clients);
        }
    }
}
