<?php

namespace Ls\GasStationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\GasStationBundle\Form\OrderListSearchAdminType;
use \Ls\GasStationBundle\Helper\OrderSearchQuery;

class OrderAdminController extends Controller
{
    private $pager_limit_name = 'admin_gas_station_order_pager_limit';

    public function indexAction(Request $request)
    {
        $authorizationChecker = $this->get('security.authorization_checker');
        $user = $this->getUser();
        $isNormalUser = $authorizationChecker->isGranted('ROLE_USER') &&
                !$authorizationChecker->isGranted('ROLE_ADMIN');
        $isNormalUserWithClient = $isNormalUser && !empty($user->getClient());
        $isAdminUser = $authorizationChecker->isGranted('ROLE_ADMIN');
        $form = $this->createForm(OrderListSearchAdminType::class, null, array(
            'method' => 'GET',
        ));
        $form->handleRequest($request);

        $session = $this->container->get('session');

        $page = $request->query->get('page', 1);
        if ($session->has($this->pager_limit_name)) {
            $limit = $session->get($this->pager_limit_name);
        } else {
            $limit = 15;
            $session->set($this->pager_limit_name, $limit);
        }
        
        $entities = array();
        
        if ($isAdminUser || $isNormalUserWithClient) {
            $paginator = $this->get('knp_paginator');
            $query = new OrderSearchQuery($this->getDoctrine(), $paginator, $isNormalUserWithClient, $user);
            
            if ($form->isValid()) {
                $query->searchBy($form->getData());
            }
            
            $entities = $query->getResults($page, $limit);
            $entities->setTemplate('LsCoreBundle:Backend:paginator.html.twig');

            if ($page > $entities->getPageCount() && $entities->getPageCount() > 0) {
                return $this->redirect($this->generateUrl('ls_gas_station_order_list_admin'));
            }
        }

        if (!$isNormalUser) {
            $breadcrumbs = $this->get('white_october_breadcrumbs');
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Zamówienia', $this->get('router')->generate('ls_gas_station_order_list_admin'));
        }

        return $this->render('LsGasStationBundle:Admin\Order:index.html.twig', array(
            'page' => $page,
            'limit' => $limit,
            'entities' => $entities,
            'form' => $form->createView(),
        ));
    }

    public function setLimitAction(Request $request)
    {
        $session = $this->container->get('session');

        $limit = $request->request->get('limit');
        $session->set($this->pager_limit_name, $limit);

        return new Response('OK');
    }
}
