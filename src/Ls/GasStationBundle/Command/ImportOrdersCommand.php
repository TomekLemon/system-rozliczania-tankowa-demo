<?php

namespace Ls\GasStationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Ls\GasStationBundle\Entity\Client;
use Ls\GasStationBundle\Entity\Product;
use Ls\GasStationBundle\Entity\Order;
use Ls\GasStationBundle\Entity\LineItem;
use League\Flysystem\Plugin\ListWith;
use League\Flysystem\Plugin\ListPaths;

class ImportOrdersCommand extends ContainerAwareCommand
{
    private $doctrine;
    private $logs;
    private $output;
    private $logsDirPath;
    private $ftp;

    protected function configure()
    {
        $this
            ->setName('orders:import')
            ->setDescription('Import zamównień z ftpa.')
            ->addArgument('maxFilesCount', InputArgument::REQUIRED, 'Ilość plików do pobrania');

        $this->logsDirPath = __DIR__ .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . '..' .
            DIRECTORY_SEPARATOR . 'app' .
            DIRECTORY_SEPARATOR . 'logs' .
            DIRECTORY_SEPARATOR . 'orders-import';
        
        if (!is_dir($this->logsDirPath)) {
            mkdir($this->logsDirPath, 777, true);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->doctrine = $this->getContainer()->get('doctrine');
        $this->logs = array();
        $this->output = $output;
        $maxFilesCount = intval($input->getArgument('maxFilesCount'));
        $filesProcessed = 0;
        
        $this->initFtp();
        $this->addLogInfo('Starting...');
        $this->addLogInfo("maxFilesCount: {$maxFilesCount}");
        
        $files = $this->ftp->listWith(['mimetype', 'size', 'timestamp']);
        
        foreach ($files as $fileInfo) {
            $parsed = $this->parseFile($fileInfo);
            
            if (!$parsed) {
                continue;
            }
            
            ++$filesProcessed;

            if ($filesProcessed >= $maxFilesCount) {
                break;
            }
        }

        $this->addLogInfo("filesProcessed: {$filesProcessed}");
        $this->addLogInfo('end.');
        $this->clearOldLogs();
        $this->saveLogs();
    }
    
    private function parseFile($fileInfo)
    {
        $em = $this->doctrine->getManager();
        $log = "basename: {$fileInfo['basename']}; size: {$fileInfo['size']}; timestamp: {$fileInfo['timestamp']}";
        $fileContents = $this->ftp->read($fileInfo['basename']);
        $fileData = json_decode($fileContents, true);
        $orderDocumentExternalId = $fileData['Dane'][0]['Dokument'];

        if ($this->orderExists($orderDocumentExternalId)) {
            $this->addLogInfo("ALREADY EXISTS - $log, with external id: {$orderDocumentExternalId}");
            $this->ftp->delete($fileInfo['basename']);

            return false;
        }

        $this->addLogInfo("ADDING - $log");
        $this->saveOrder($orderDocumentExternalId, $fileData, $em);
        $this->ftp->delete($fileInfo['basename']);
        
        return true;
    }
    
    private function saveOrder($orderDocumentExternalId, $fileData, $em)
    {
        $order = new Order();
        $order->setDocumentExternalId($orderDocumentExternalId);
        $order->setCarRegistrationNumber($fileData['Dane'][0]['Nr_Rejestracyjny']);
        $order->setDate(\DateTime::createFromFormat(\DateTime::ISO8601, $fileData['Dane'][0]['DataS']));
        $order->setDriverFullName($fileData['Dane'][0]['Kierowca']);

        foreach ($fileData['Dane'][0]['Pozycje_Dok'] as $exportedItem) {
            $order->addLineItem($this->saveLineItem($em, $exportedItem));
        }

        $clientNip = $fileData['Dane'][0]['Kontrahent_NIP'];
        $client = $this->getClient($clientNip);

        if (empty($client)) {
            $client = new Client();
            $client->setNip($clientNip);
            $em->persist($client);
        }
        $client->addOrder($order);
        $em->flush();
    }
    
    private function saveLineItem($em, $exportedItem)
    {
        $productId = trim($exportedItem['Towar_Index']);
        $product = $this->getProduct($productId);
        $lineItem = new LineItem();

        if (empty($product)) {
            $product = new Product();
            $product->setExternalId($productId);
            $product->setName($exportedItem['Towar_Nazwa']);
            $em->persist($product);
        }

        $lineItem->setProduct($product);
        $lineItem->setQuantity($exportedItem['Ilosc']);
        $lineItem->setTotalPriceNet($exportedItem['Wartosc_Netto']);
        $lineItem->setTotalPriceGross($exportedItem['Wartosc_Brutto']);
        
        return $lineItem;
    }
    
    private function initFtp()
    {
        $this->ftp = $this->getContainer()->get('oneup_flysystem.ftp_with_orders_filesystem');
        $this->ftp->addPlugin(new ListWith());
        $this->ftp->addPlugin(new ListPaths());
    }

    private function orderExists($documentExternalId)
    {
        $em = $this->doctrine->getManager();

        $ordersCount = $em->createQueryBuilder()
            ->select('count(o.id)')
            ->from('LsGasStationBundle:Order', 'o')
            ->where('o.documentExternalId = :documentExternalId')
            ->setParameter('documentExternalId', $documentExternalId)
            ->getQuery()
            ->getSingleScalarResult();

        return $ordersCount > 0;
    }

    private function getClient($nip)
    {
        $em = $this->doctrine->getManager();

        return $em->createQueryBuilder()
                ->select('c')
                ->from('LsGasStationBundle:Client', 'c')
                ->where('c.nip = :nip')
                ->setParameter('nip', $nip)
                ->getQuery()
                ->getOneOrNullResult();
    }

    private function getProduct($externalId)
    {
        $em = $this->doctrine->getManager();

        return $em->createQueryBuilder()
                ->select('p')
                ->from('LsGasStationBundle:Product', 'p')
                ->where('p.externalId = :externalId')
                ->setParameter('externalId', $externalId)
                ->getQuery()
                ->getOneOrNullResult();
    }

    private function addLogInfo($text, $showInCli = true)
    {
        if ($showInCli) {
            $this->output->writeln($text);
        }

        $this->logs[] = $text;
    }

    private function clearOldLogs()
    {
        $fs = new Filesystem();
        $finder = new Finder();
        $files = $finder->files()
            ->ignoreUnreadableDirs()
            ->date('until 3 days ago')
            ->in($this->logsDirPath)
            ->files();

        $fs->remove($files);
    }

    private function saveLogs()
    {
        $fileName = 'orders-import-log' . time() . '.txt';
        $filePath = $this->logsDirPath . DIRECTORY_SEPARATOR . $fileName;
        $logFile = new \SplFileObject($filePath, 'c+');
        $logMessage = '';

        foreach ($this->logs as $log) {
            $logMessage .= $log . PHP_EOL;
        }

        $logFile->fwrite($logMessage);
    }
}
