<?php

namespace Ls\GasStationBundle\Helper;

class OrderSearchQuery
{
    private $doctrine;
    private $queryBuilder;
    private $queryWhereExpressions;
    private $queryParameters;
    private $knpPaginator;
    
    public function __construct($doctrine, $knpPaginator, $isNormalUserWithClient, $user)
    {
        $this->doctrine = $doctrine;
        $this->queryBuilder = $doctrine
            ->getManager()
            ->createQueryBuilder();
        $this->queryWhereExpressions = array();
        $this->queryParameters = array();
        $this->knpPaginator = $knpPaginator;
        
        if ($isNormalUserWithClient) {
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->eq('c.id', ':clientId');
            $this->queryParameters['clientId'] = $user->getClient()->getId();
        }
    }
    
    public function searchBy($searchValues)
    {
        if (empty($searchValues)) {
            return;
        }
        
        if (!empty($searchValues['clientName'])) {
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->like('c.name', ':clientName');
            $this->queryParameters['clientName'] = "%{$searchValues['clientName']}%";
        }

        if (!empty($searchValues['carRegistrationNumber'])) {
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->like('o.carRegistrationNumber', ':carRegistrationNumber');
            $this->queryParameters['carRegistrationNumber'] = "%{$searchValues['carRegistrationNumber']}%";
        }

        if (!empty($searchValues['orderDateFrom'])) {
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->gte('o.date', ':orderDateFrom');
            $this->queryParameters['orderDateFrom'] = $searchValues['orderDateFrom'];
        }

        if (!empty($searchValues['orderDateTo'])) {
            $this->queryWhereExpressions[] = $this->queryBuilder->expr()->lte('o.date', ':orderDateTo');
            $this->queryParameters['orderDateTo'] = $searchValues['orderDateTo'];
        }
    }
    
    public function getResults($pageNumber, $pageSize)
    {
        $query = $this->getQuery();
        $pagination = $this->knpPaginator->paginate(
            $query, $pageNumber, $pageSize, array(
            'defaultSortFieldName' => 'o.date',
            'defaultSortDirection' => 'desc',
        ));
        $lineItems = $this->getLineItems($pagination->getItems());
        
        foreach ($pagination->getItems() as $order) {
            $this->addLineItemsToOrder($order, $lineItems);
        }
        
        return $pagination;
    }
    
    private function addLineItemsToOrder($order, $lineItems)
    {
        foreach ($lineItems as $lineItem) {
            if ($lineItem->getOrder()->getId() === $order->getId()) {
                $order->addLineItem($lineItem);
            }
        }
    }
    
    private function getLineItems($orders)
    {
        $orderIds = array_map(function ($order) {
            return $order->getId();
        }, $orders);
        
        return $this->doctrine
            ->getManager()
            ->createQueryBuilder()
            ->select('partial li.{id, quantity, totalPriceGross, totalPriceNet}, partial p.{id, name}, partial o.{id}')
            ->from('LsGasStationBundle:LineItem', 'li')
            ->leftJoin('li.product', 'p')
            ->leftJoin('li.order', 'o')
            ->where('o.id IN(:orderIds)')
            ->setParameter('orderIds', $orderIds)
            ->getQuery()
            ->getResult();
    }
    
    private function concatenateWhereExpressions() 
    {
        $queryWhereFullExpression = $this->queryBuilder->expr()->andX();
        
        foreach ($this->queryWhereExpressions as $expression) {
            $queryWhereFullExpression->add($expression);
        }
        
        return $queryWhereFullExpression;
    }
    
    private function getQuery()
    {
        $ordersCountQuery = $this->doctrine
            ->getManager()
            ->createQueryBuilder()
            ->select('COUNT(o.id)')
            ->from('LsGasStationBundle:Order', 'o')
            ->leftJoin('o.client', 'c');
        
        $ordersQuery = $this->doctrine
            ->getManager()
            ->createQueryBuilder()
            ->select('partial o.{id, documentExternalId, driverFullName, carRegistrationNumber, date}, partial c.{id, name, nip}')
            ->from('LsGasStationBundle:Order', 'o')
            ->leftJoin('o.client', 'c');
        
        if (count($this->queryWhereExpressions) > 0) {
            $queryWhereFullExpression = $this->concatenateWhereExpressions();
            $ordersCountQuery = $ordersCountQuery
                ->add('where', $queryWhereFullExpression)
                ->setParameters($this->queryParameters);
            $ordersQuery = $ordersQuery
                ->add('where', $queryWhereFullExpression)
                ->setParameters($this->queryParameters);
        }
        
        $ordersCount = $ordersCountQuery
            ->getQuery()
            ->getSingleScalarResult();
        
        return $ordersQuery
            ->getQuery()
            ->setHint('knp_paginator.count', $ordersCount)
            ->setHint(\Doctrine\ORM\Query::HINT_FORCE_PARTIAL_LOAD, true);
    }
}
