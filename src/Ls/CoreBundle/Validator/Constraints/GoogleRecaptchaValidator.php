<?php

namespace Ls\CoreBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * @Annotation
 */
class GoogleRecaptchaValidator extends ConstraintValidator
{
    const RECAPTCHA_VERIFY_SERVER = 'https://www.google.com';

    private $secretKey;
    private $request;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->secretKey = $container->get('cms_config')->get('google_recaptcha_secret_key', '');
    }

    public function validate($value, Constraint $constraint)
    {
        $isValid = $this->checkAnswer();

        if (!$isValid) {
            $this->context->addViolation($constraint->message, array());
        }
    }

    private function checkAnswer()
    {
        $remoteIp = $this->request->server->get('REMOTE_ADDR');
        $recaptchaResponse = $this->request->get('g-recaptcha-response');

        if ($remoteIp == null || $remoteIp == '') {
            throw new ValidatorException('For security reasons, you must pass the remote ip to reCAPTCHA');
        }

        // discard spam submissions
        if ($recaptchaResponse == null || strlen($recaptchaResponse) == 0) {
            return false;
        }

        $verifyServerResponse = $this->httpGet(self::RECAPTCHA_VERIFY_SERVER, '/recaptcha/api/siteverify', array(
            'secret' => $this->secretKey,
            'remoteip' => $remoteIp,
            'response' => $recaptchaResponse,
        ));

        $decodedVerifyServerResponse = json_decode($verifyServerResponse, true);

        if ($decodedVerifyServerResponse['success'] == true) {
            return true;
        }

        return false;
    }

    private function httpGet($host, $path, $data)
    {
        $host = sprintf('%s%s?%s', $host, $path, http_build_query($data));

        return file_get_contents($host);
    }
}
