<?php

namespace Ls\CoreBundle\Helper;

class AdminBlock
{
    private $name;
    private $rows;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function addRow(AdminRow $row)
    {
        $this->rows[] = $row;
    }
}
