<?php

namespace Ls\CoreBundle\Helper;

class AdminLink
{
    private $name;
    private $icon;
    private $url;

    public function __construct($name, $icon, $url)
    {
        $this->name = $name;
        $this->icon = $icon;
        $this->url = $url;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }
}
