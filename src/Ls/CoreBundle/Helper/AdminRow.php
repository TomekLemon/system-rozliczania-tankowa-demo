<?php

namespace Ls\CoreBundle\Helper;

class AdminRow
{
    private $name;
    private $links;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLinks()
    {
        return $this->links;
    }

    public function addLink(AdminLink $link)
    {
        $this->links[] = $link;
    }
}
