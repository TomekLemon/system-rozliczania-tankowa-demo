<?php

namespace Ls\CoreBundle\Helper;

class AdminDashboard
{
    private $blocks;

    public function __construct()
    {
        $this->blocks = array();
    }

    public function getBlocks()
    {
        return $this->blocks;
    }

    public function getBlock($name)
    {
        $result = null;

        foreach ($this->blocks as $block) {
            if (strcmp($block->getName(), $name) == 0) {
                $result = $block;
            }
        }

        return $result;
    }

    public function addBlock(AdminBlock $block)
    {
        $this->blocks[] = $block;
    }
}
