<?php

namespace Ls\SettingBundle\Service;

use Knp\Menu\ItemInterface;
use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminLink;
use Ls\CoreBundle\Helper\AdminRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminService
{
    private $container;
    private $authorizationChecker;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->authorizationChecker = $container->get('security.authorization_checker');
    }

    public function addToMenu(ItemInterface $parent, $route, $set)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $item = $parent->addChild('Ustawienia', array(
            'route' => 'ls_admin_setting',
        ));

        $current_set = true;

        if (!$set) {
            switch ($route) {
                case 'ls_admin_setting':
                case 'ls_admin_setting_new':
                case 'ls_admin_setting_edit':
                case 'ls_admin_setting_batch':
                    $item->setCurrent(true);
                    break;
                default:
                    $current_set = false;
                    break;
            }
        }

        return $current_set;
    }

    public function addToDashboard(AdminBlock $parent)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return;
        }

        $router = $this->container->get('router');

        $row = new AdminRow('Ustawienia');
        $parent->addRow($row);

        $row->addLink(new AdminLink('Dodaj', 'glyphicon-plus', $router->generate('ls_admin_setting_new')));
        $row->addLink(new AdminLink('Zarządzaj', 'glyphicon-list', $router->generate('ls_admin_setting')));
    }
}
