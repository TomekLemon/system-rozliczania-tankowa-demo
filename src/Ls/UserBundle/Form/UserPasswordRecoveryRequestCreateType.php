<?php

namespace Ls\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ls\UserBundle\Validators\UserEmailExists;

class UserPasswordRecoveryRequestCreateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', null, array(
            'label' => 'Adres e-mail',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                )),
                new Email(array(
                    'message' => 'Niepoprawny adres e-mail',
                )),
                new UserEmailExists(),
            ),
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'form_user_password_recovery_request_create';
    }
}
