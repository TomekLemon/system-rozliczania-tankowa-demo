<?php

namespace Ls\UserBundle\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

/**
 * @Annotation
 */
class FreeUserEmail extends Constraint
{
    public $message = 'Podany adres email jest już zajęty.';
    public $excludedUserId;
}

class FreeUserEmailValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->userEmailExists($value, $constraint->excludedUserId)) {
            $this->context->addViolation($constraint->message);
        }
    }

    private function userEmailExists($email, $excludedUserId)
    {
        $query = $this->entityManager
                ->createQueryBuilder()
                ->select('count(u.id)')
                ->from('LsUserBundle:User', 'u')
                ->where('u.email = :email')
                ->setParameter('email', $email);

        if (!empty($excludedUserId)) {
            $query = $query
                ->andWhere('u.id != :userId')
                ->setParameter('userId', $excludedUserId);
        }

        $usersCount = $query
                ->getQuery()
                ->getSingleScalarResult();

        return $usersCount > 0;
    }
}
