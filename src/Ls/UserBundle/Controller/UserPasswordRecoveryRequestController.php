<?php

namespace Ls\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ls\UserBundle\Form\UserPasswordRecoveryRequestCreateType;
use Ls\UserBundle\Form\UserPasswordRecoveryRequestPassowrdChangeType;
use Ls\UserBundle\Entity\UserPasswordRecoveryRequest;

class UserPasswordRecoveryRequestController extends Controller
{
    public function createPasswordRecoveryRequestAction(Request $request)
    {
        $form = $this->createForm(UserPasswordRecoveryRequestCreateType::class, null, array(
            'method' => 'POST',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $user = $em
                    ->getRepository('LsUserBundle:User')
                    ->findOneByEmail($data['email']);

            $em->createQueryBuilder()
                    ->delete('LsUserBundle:UserPasswordRecoveryRequest', 'e')
                    ->where('e.user = :user')
                    ->andWhere('e.dateOfUse is null')
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->execute();

            $passwordRecoveryRequest = new UserPasswordRecoveryRequest();
            $passwordRecoveryRequest->setUser($user);
            $em->persist($passwordRecoveryRequest);
            $em->flush();

            $this->sendEmailWithPasswordRecoveryRequest($passwordRecoveryRequest);

            $this->get('session')->getFlashBag()->add('success', 'Na podany adres e-mail została wysłana wiadomość z linkiem do zmiany hasła.');

            return $this->redirect($this->generateUrl('ls_user_password_recovery_request_create'));
        } elseif ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        return $this->render('LsUserBundle:UserPasswordRecoveryRequest:requestCreate.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function changePasswordUsingRecoveryRequestAction(Request $request, $code)
    {
        $passwordRecoveryRequest = $this->getPasswordRecoveryRequest($code);

        if (empty($passwordRecoveryRequest)) {
            return $this->redirect($this->generateUrl('ls_user_password_recovery_request_create'));
        }
        // query sprawdzajace czy code jest prawidlowy - jesli nie to wyswietlenie samego info
        // sprawdzenie formularza czy hasla sie zgadzaja
        // zmiana hasla
        // wyswietlenie odpowiedniego info userowi
        // wyswietlenie linku do strony logowania

        $form = $this->createForm(UserPasswordRecoveryRequestPassowrdChangeType::class, null, array(
            'method' => 'POST',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $passwordEncoder = $this->container->get('security.password_encoder');
            $encodedPassword = $passwordEncoder->encodePassword($passwordRecoveryRequest->getUser(), $data['password']);
            $passwordRecoveryRequest->getUser()->setPassword($encodedPassword);
            $passwordRecoveryRequest->setDateOfUse(new \DateTime());
            $em->flush();

            $this->get('session')->getFlashBag()->add('info', 'Hasło zostało poprawnie zmienione. Teraz możesz się zalogować.');

            return $this->redirect($this->generateUrl('ls_core_admin'));
        } elseif ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        return $this->render('LsUserBundle:UserPasswordRecoveryRequest:passwordChange.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    private function sendEmailWithPasswordRecoveryRequest($passwordRecoveryRequest)
    {
        $subject = 'AutoMarian - resetowanie hasła';
        $passwordChangeUrl = $this->generateUrl(
                'ls_user_password_recovery_request_password_change',
                array('code' => $passwordRecoveryRequest->getCode()), true);

        $message = 'Dzień dobry,<br /><br />';
        $message .= 'Ten email jest odpowiedzią na prośbę o zresetowanie hasła.<br />';
        $message .= 'Klikając w poniższy link zostaniesz przeniesiony/a do strony, ';
        $message .= 'na której będziesz mógł/mogła zmienić hasło. ';
        $message .= '<br />';
        $message .= '<a href="'.$passwordChangeUrl.'">Link do zmiany hasła</a>';

        $email = \Swift_Message::newInstance();
        $email->setSubject($subject);
        $email->setFrom(array($this->container->getParameter('mailer_user') => $this->container->getParameter('mailer_name')));
        $email->setTo($passwordRecoveryRequest->getUser()->getEmail());
        $email->setBody($message, 'text/html');
        $email->addPart(strip_tags($message), 'text/plain');

        $mailer = $this->get('mailer');
        $mailer->send($email);
    }

    private function getPasswordRecoveryRequest($code)
    {
        $em = $this->getDoctrine()->getManager();

        return $em->createQueryBuilder()
            ->select('e, u')
            ->from('LsUserBundle:UserPasswordRecoveryRequest', 'e')
            ->join('e.user', 'u')
            ->where('e.code = :code')
            ->andWhere('e.expirationDate >= :now')
            ->andWhere('e.dateOfUse is null')
            ->setParameter('code', $code)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }
}
