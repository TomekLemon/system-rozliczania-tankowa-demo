<?php

namespace Ls\UserBundle\Controller;

use Ls\UserBundle\Entity\User;
use Ls\UserBundle\Form\ClientAccountEditType;
use Ls\UserBundle\Form\ClientAccountChangePasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ClientAccountAdminController extends Controller
{
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if (empty($user->getClient())) {
            throw $this->createAccessDeniedException('Unable to access this page!');
        }
        
        $form = $this->createForm(ClientAccountEditType::class, array(
                'clientName' => $user->getClient()->getName(),
                'email' => $user->getEmail(),
            ), array(
                'method' => 'POST',
                'excludedUserId' => $user->getId(),
            ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $user->getClient()->setName($data['clientName']);
            $user->setEmail($data['email']);
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja użytkownika zakończona sukcesem.');

            return $this->redirect($this->generateUrl('ls_user_client_account_edit_admin'));
        }

        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Edycja konta', '');

        return $this->render('LsUserBundle:Admin\ClientAccount:edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    public function changePasswordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if (empty($user->getClient())) {
            throw $this->createAccessDeniedException('Unable to access this page!');
        }

        $form = $this->createForm(ClientAccountChangePasswordType::class, null, array(
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $passwordEncoder = $this->container->get('security.password_encoder');
            $encodedPassword = $passwordEncoder->encodePassword($user, $data['password']);
            $user->setPassword($encodedPassword);
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Zmiana hasła zakończona sukcesem.');

            return $this->redirect($this->generateUrl('ls_user_client_account_change_password_admin'));
        }

        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get('white_october_breadcrumbs');
        $breadcrumbs->addItem('Zmiana hasła', '');

        return $this->render('LsUserBundle:Admin\ClientAccount:changePassword.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
