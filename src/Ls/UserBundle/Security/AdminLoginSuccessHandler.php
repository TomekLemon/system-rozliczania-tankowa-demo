<?php

namespace Ls\UserBundle\Security;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminLoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $router;
    private $authorizationChecker;

    public function __construct($container)
    {
        $this->router = $container->get('router');
        $this->authorizationChecker = $container->get('security.authorization_checker');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            return new RedirectResponse($this->router->generate('ls_core_admin'));
        }

        return new RedirectResponse($this->router->generate('ls_gas_station_order_list_admin'));
    }
}
