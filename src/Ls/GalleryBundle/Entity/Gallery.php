<?php

namespace Ls\GalleryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\PageBundle\Entity\Page;

/**
 * Gallery.
 *
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $on_list;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $attachable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @var bool
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="GalleryPhoto",
     *   mappedBy="gallery",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"arrangement" = "ASC"})
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Ls\PageBundle\Entity\Page",
     *   mappedBy="gallery"
     * )
     *
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pages;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->on_list = true;
        $this->attachable = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->photos = new ArrayCollection();
        $this->pages = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Gallery
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Gallery
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set old_slug.
     *
     * @param string $old_slug
     *
     * @return Gallery
     */
    public function setOldSlug($old_slug)
    {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug.
     *
     * @return string
     */
    public function getOldSlug()
    {
        return $this->old_slug;
    }

    /**
     * Set on_list.
     *
     * @param bool $on_list
     *
     * @return Gallery
     */
    public function setOnList($on_list)
    {
        $this->on_list = $on_list;

        return $this;
    }

    /**
     * Get on_list.
     *
     * @return bool
     */
    public function getOnList()
    {
        return $this->on_list;
    }

    /**
     * Set attachable.
     *
     * @param bool $attachable
     *
     * @return Gallery
     */
    public function setAttachable($attachable)
    {
        $this->attachable = $attachable;

        return $this;
    }

    /**
     * Get attachable.
     *
     * @return bool
     */
    public function getAttachable()
    {
        return $this->attachable;
    }

    /**
     * Set seo_generate.
     *
     * @param bool $seo_generate
     *
     * @return Gallery
     */
    public function setSeoGenerate($seo_generate)
    {
        $this->seo_generate = $seo_generate;

        return $this;
    }

    /**
     * Get seo_generate.
     *
     * @return bool
     */
    public function getSeoGenerate()
    {
        return $this->seo_generate;
    }

    /**
     * Set seo_title.
     *
     * @param string $seo_title
     *
     * @return Gallery
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * Get seo_title.
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords.
     *
     * @param string $seo_keywords
     *
     * @return Gallery
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;

        return $this;
    }

    /**
     * Get seo_keywords.
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description.
     *
     * @param string $seo_description
     *
     * @return Gallery
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;

        return $this;
    }

    /**
     * Get seo_description.
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * Set created_at.
     *
     * @param \DateTime $created_at
     *
     * @return Gallery
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at.
     *
     * @param \DateTime $updated_at
     *
     * @return Gallery
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Add photo.
     *
     * @param \Ls\GalleryBundle\Entity\GalleryPhoto $photo
     *
     * @return Gallery
     */
    public function addPhoto(GalleryPhoto $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo.
     *
     * @param \Ls\GalleryBundle\Entity\GalleryPhoto $photo
     */
    public function removePhoto(GalleryPhoto $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add page.
     *
     * @param \Ls\PageBundle\Entity\Page $page
     *
     * @return Gallery
     */
    public function addPage(Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page.
     *
     * @param \Ls\PageBundle\Entity\Page $page
     */
    public function removePage(Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }
}
