-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               10.1.19-MariaDB - mariadb.org binary distribution
-- Serwer OS:                    Win32
-- HeidiSQL Wersja:              9.4.0.5137
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela autamarianademo.client
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C7440455A76ED395` (`user_id`),
  CONSTRAINT `FK_C7440455A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.client: ~2 rows (około)
DELETE FROM `client`;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`id`, `nip`, `created_at`, `updated_at`, `name`, `user_id`) VALUES
	(1, '9999999999', '2016-05-13 13:15:35', NULL, 'Test', NULL),
	(2, '7777777777', '2017-01-11 16:03:15', NULL, 'Drugi klient', NULL),
	(3, '6666666666', '2017-01-12 13:16:54', NULL, 'Klient 3', 5);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.contact: ~0 rows (około)
DELETE FROM `contact`;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.gallery: ~0 rows (około)
DELETE FROM `gallery`;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.gallery_photo
CREATE TABLE IF NOT EXISTS `gallery_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F02A543B4E7AF8F` (`gallery_id`),
  CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.gallery_photo: ~0 rows (około)
DELETE FROM `gallery_photo`;
/*!40000 ALTER TABLE `gallery_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_photo` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.line_item
CREATE TABLE IF NOT EXISTS `line_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `total_price_net` decimal(10,2) NOT NULL,
  `total_price_gross` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9456D6C78D9F6D38` (`order_id`),
  KEY `IDX_9456D6C74584665A` (`product_id`),
  CONSTRAINT `FK_9456D6C74584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_9456D6C78D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=495 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.line_item: ~2 rows (około)
DELETE FROM `line_item`;
/*!40000 ALTER TABLE `line_item` DISABLE KEYS */;
INSERT INTO `line_item` (`id`, `order_id`, `product_id`, `quantity`, `total_price_net`, `total_price_gross`, `created_at`, `updated_at`) VALUES
	(493, 467, 3, 275.01, 990.48, 1218.29, '2016-12-08 05:00:21', NULL),
	(494, 468, 2, 60.00, 228.29, 280.80, '2016-12-08 10:50:34', NULL);
/*!40000 ALTER TABLE `line_item` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.migration_versions
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.migration_versions: ~4 rows (około)
DELETE FROM `migration_versions`;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` (`version`) VALUES
	('20160506124827'),
	('20160510134256'),
	('20160512171634'),
	('20160517171532');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.order
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `document_external_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `document_number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `gas_station_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `document_symbol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `car_registration_number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `driver_full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F529939819EB6921` (`client_id`),
  CONSTRAINT `FK_F529939819EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=469 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.order: ~2 rows (około)
DELETE FROM `order`;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `client_id`, `document_external_id`, `document_number`, `gas_station_id`, `document_symbol`, `car_registration_number`, `date`, `created_at`, `updated_at`, `driver_full_name`) VALUES
	(467, 1, '1111/1/WZB', '1111', '1', 'WZB', 'RZE 00000', '2016-12-08 04:57:29', '2016-12-08 05:00:21', NULL, 'KOWALSKI JAN'),
	(468, 1, '1111/1/WZB', '1111', '1', 'WZB', 'RZE 00000', '2016-12-08 10:51:46', '2016-12-08 10:50:34', NULL, 'NOWAK JAN');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.page
CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_140AB6204E7AF8F` (`gallery_id`),
  CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.page: ~0 rows (około)
DELETE FROM `page`;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
/*!40000 ALTER TABLE `page` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.product: ~6 rows (około)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `external_id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'LPG', 'LPG', '2016-05-13 18:00:16', NULL),
	(2, 'PB95', 'PB95 BENZYNA BEZOŁOWIOWA', '2016-05-16 11:00:18', NULL),
	(3, 'ON', 'ON OLEJ NAPĘDOWY', '2016-05-16 13:20:22', NULL),
	(4, 'BUTLA GAZ 11KG', 'BUTLA GAZ 11KG', '2016-05-17 13:00:16', NULL),
	(5, 'OLEJ OPEL 10W40 1L', 'OLEJ OPEL 10W40 1L', '2016-07-28 10:48:56', NULL),
	(6, 'OLEJ HYDRAULICZNY 5L', 'OLEJ HYDRAULICZNY LHL-46 5L', '2016-11-30 15:40:17', NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.setting: ~6 rows (około)
DELETE FROM `setting`;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
	(1, 'seo_description', 'Domyślna wartość meta tagu "description"', NULL),
	(2, 'seo_keywords', 'Domyślna wartość meta tagu "keywords"', NULL),
	(3, 'seo_title', 'Domyślna wartość meta tagu "title"', NULL),
	(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', NULL),
	(5, 'google_recaptcha_secret_key', 'Google reCAPTCHA - secret key', NULL),
	(6, 'google_recaptcha_site_key', 'Google reCAPTCHA - site key', NULL);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.user: ~2 rows (około)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '["ROLE_ADMIN","ROLE_ALLOWED_TO_SWITCH"]', '2015-07-31 13:46:34', NULL),
	(5, 'test', 'test@test.pl', '2847d23bc9ca507a6d7549ae0b6eaf3a', 'uBFuKnXjQCQuje0/ExwSyMT8AHocc/eKAMjJ9lcJ3B+O04a+r4NIC46n91gokfoMmkDpCIs/EcSm1oer18pclA==', 1, NULL, '["ROLE_ADMIN"]', '2016-12-09 13:00:09', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Zrzut struktury tabela autamarianademo.user_password_recovery_request
CREATE TABLE IF NOT EXISTS `user_password_recovery_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `expiration_date` datetime NOT NULL,
  `date_of_use` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AD70E351A76ED395` (`user_id`),
  CONSTRAINT `FK_AD70E351A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli autamarianademo.user_password_recovery_request: ~0 rows (około)
DELETE FROM `user_password_recovery_request`;
/*!40000 ALTER TABLE `user_password_recovery_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_password_recovery_request` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
