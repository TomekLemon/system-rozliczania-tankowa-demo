# Doctrine Migration File Generated on 2016-05-12 17:17:06

# Version 20160512171634
CREATE TABLE user_password_recovery_request (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, code VARCHAR(40) NOT NULL, expiration_date DATETIME NOT NULL, date_of_use DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_AD70E351A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE user_password_recovery_request ADD CONSTRAINT FK_AD70E351A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE;
ALTER TABLE `order` CHANGE date date DATETIME NOT NULL;
INSERT INTO migration_versions (version) VALUES ('20160512171634');
